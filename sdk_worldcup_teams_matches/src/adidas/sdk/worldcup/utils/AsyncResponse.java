package adidas.sdk.worldcup.utils;

import org.json.JSONArray;

/**
 * Declaration of the interface. This is responsible to advice to the request
 * of this sdk when the data is loaded
 * @author jesusmiguel
 *
 */
public interface AsyncResponse {
    void processFinish(JSONArray output);
}