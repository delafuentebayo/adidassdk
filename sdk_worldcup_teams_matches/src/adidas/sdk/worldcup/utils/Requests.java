package adidas.sdk.worldcup.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import adidas.sdk.worldcup.classes.Match;
import adidas.sdk.worldcup.classes.Team;

/**
 * Method responsible to create the requests from the main app.
 * It will return the information of the matches or teams sorted.
 * It will process the JSONArray and return the list of matches and teams
 * @author jesusmigueldelafuente
 *
 */
public class Requests implements AsyncResponse {
	//Declaracion of the callback
	public SdkResponse callback = null;
	//Url to get the data
	private String url_to_visit;
	//Lists of matches and teams to be returned
	private ArrayList<Match> matches;
	private ArrayList<Team> teams;

	public Requests() {
		super();

	}

	/**
	 * Constructor with parameter to define
	 * the link to get the data from. In order
	 * to avoid the outside developers to check
	 * the url where is connected, it will be necessary
	 * to pass "matches" or "teams" and the sdk will define
	 * where to connect
	 * @param url_to_visit
	 */
	public Requests(String url_to_visit) {
		super();
		this.url_to_visit = url_to_visit;
		if (url_to_visit.equalsIgnoreCase(Globals.MATCHES)) {
			//We call to the AsyncTask to get the JSONArray
			Communications comm = new Communications(Globals.URL_MATCHES);
			comm.delegate=this;
			comm.execute();
		} else if (url_to_visit.equalsIgnoreCase(Globals.TEAMS)) {
			//We call to the AsyncTask to get the JSONArray
			Communications comm = new Communications(Globals.URL_TEAMS);
			comm.delegate=this;
			comm.execute();
		}
	}

	/**
	 * It returns the list of the matches sorted
	 * @return list of sorted matches
	 */
	public ArrayList<Match> getMatches() {
		sortListMatch();
		return matches;
	}
	

	/**
	 * It returns the list of teams sorted
	 * @return list of sorted teams
	 */
	public ArrayList<Team> getTeams() {
		sortListTeam();
		return teams;
	}
	
	/**
	 * Method called from the AsyncTask responsible to get the data
	 * once is finished.
	 */
	@Override
	public void processFinish(JSONArray output) {
		//Depending on the url defined, it will parse
		//in one way or other the JSON
		if (url_to_visit.equalsIgnoreCase(Globals.MATCHES)) {
			processMatches(output);

		} else if (url_to_visit.equalsIgnoreCase(Globals.TEAMS)) {
			processTeams(output);

		}
		callback.processFinish();
	}

	/**
	 * Method which process the JSON with the teams
	 * @param output JSONArray
	 */
	private void processTeams(JSONArray output) {
		teams = new ArrayList<Team>();
		try {
			JSONArray jsonArray = output;
			//We check all the items in the array
			for (int i = 0; i < jsonArray.length(); i++) {
				Team team = new Team();
				//Created a new team, we are going to fill the data
				JSONObject curr = jsonArray.getJSONObject(i);
				team.setCountry(curr.getString(Globals.TEAMS_JSON_COUNTRY));
				team.setAlternate_name(curr.getString(Globals.TEAMS_JSON_ALTERNATE_NAME));
				team.setFifa_code(curr.getString(Globals.TEAMS_JSON_FIFA_CODE));
				team.setGroup_id(curr.getString(Globals.TEAMS_JSON_GROUP_ID));
				teams.add(team);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Method to process de JSONArray for matches
	 * @param output JSONArray
	 */
	private void processMatches(JSONArray output) {
		matches = new ArrayList<Match>();
		try {
			JSONArray jsonArray = output;
			//We check all the items in the array

			for (int i = 0; i < jsonArray.length(); i++) {
				Match match = new Match();
				JSONObject curr = jsonArray.getJSONObject(i);
				//Created a new match, we are going to fill the data

				match.setMatch_number(curr.getInt(Globals.MATCHES_JSON_NUMBER));
				match.setLocation(curr.getString(Globals.MATCHES_JSON_LOCATION));
				match.setStatus(curr.getString(Globals.MATCHES_JSON_STATUS));
				JSONObject jsonTeam = curr.getJSONObject(Globals.MATCHES_JSON_HOME_TEAM);
				//We find an object inside the array, so we process it
                Team home_team = new Team();
                	//Is a new team (home)
                    home_team.setCountry(jsonTeam.getString(Globals.MATCHES_JSON_COUNTRY));
                    home_team.setFifa_code(jsonTeam.getString(Globals.MATCHES_JSON_COUNTRY_CODE));
                    match.setHome_goals(jsonTeam.getInt(Globals.MATCHES_JSON_COUNTRY_GOALS));
                    match.setHome_team(home_team);
                  
                    //Is another team (away)
                    JSONObject jsonTeam2 = curr.getJSONObject(Globals.MATCHES_JSON_AWAY_TEAM);
				
                    Team away_team = new Team();
                    away_team.setCountry(jsonTeam2.getString(Globals.MATCHES_JSON_COUNTRY));
                    away_team.setFifa_code(jsonTeam2.getString(Globals.MATCHES_JSON_COUNTRY_CODE));
                    match.setAway_goals(jsonTeam2.getInt(Globals.MATCHES_JSON_COUNTRY_GOALS));
                    match.setAway_team(away_team);
                  
                

				matches.add(match);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Method responsible to short the teams
	 */
	public void sortListTeam() 
	{
		
			
			Collections.sort(teams, new Comparator<Team>() 
				{
				public int compare(Team team1, Team team2) 
				{
					String desc1 = team1.getGroup_id();
					if (desc1 == null)
						desc1 = "";
					String desc2 = team2.getGroup_id();
					if (desc2 == null)
						desc2 = "";

					return desc1.compareTo(desc2);
				}
			});
	
	}
	
	/**
	 * Method responsible to sort the Match
	 */
	public void sortListMatch() 
	{
		
			
			Collections.sort(matches, new Comparator<Match>() 
				{
				public int compare(Match match1, Match match2) 
				{
					String desc1 = String.valueOf(match1.getMatch_number());
					if (desc1 == null)
						desc1 = "";
					String desc2 = String.valueOf(match2.getMatch_number());
					if (desc2 == null)
						desc2 = "";

					return desc1.compareTo(desc2);
				}
			});
	
	}


}
