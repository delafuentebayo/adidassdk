package adidas.sdk.worldcup.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Class of communications, responsible to get the data from the internet. It
 * extends an AsyncTask to load the data out of the mainThread, following Google
 * rules.
 * 
 * @author jesusmiguel
 * 
 */
public class Communications extends AsyncTask<Void, Void, JSONArray> {
	// Declaration of the callback to return when the process has finished
	public AsyncResponse delegate = null;
	// url where is going to connect
	String rest_uri;

	/**
	 * Constructor with url
	 * 
	 * @param uri
	 */
	public Communications(String uri) {
		rest_uri = uri;
	}

	/**
	 * Method called when the task has finished
	 */
	@Override
	protected void onPostExecute(JSONArray result) {
		// the callback will notify to that method that it has finished
		delegate.processFinish(result);

	}

	/**
	 * Method called while the task is running
	 */
	@Override
	protected JSONArray doInBackground(Void... params) {

		JSONArray textResponse = getStatusResponseFromServer();

		return textResponse;

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	/**
	 * Method responsible to get the JSONArray from the Server
	 * 
	 * @return
	 */
	public JSONArray getStatusResponseFromServer() {

		String result = "";
		JSONArray obj = new JSONArray();
		try {
			// We create a GET connection in this case
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(rest_uri);

			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpResponse.getEntity();
			// We receive the result in a String
			result = EntityUtils.toString(httpEntity);
			// We convert the String in JSONArray
			obj = new JSONArray(result);
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("Adidas Log: ", "Could not parse malformed JSON: \"" + e
					+ "\"");

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return obj;
	}

}
