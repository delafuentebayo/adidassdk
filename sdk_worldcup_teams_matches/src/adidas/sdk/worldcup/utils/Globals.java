package adidas.sdk.worldcup.utils;

/**
 * Class to define global definitions
 * for the sdk (url, value keys for the json, etc.)
 * @author jesusmiguel
 *
 */
class Globals {
	
	protected static final String URL_MATCHES="http://worldcup.sfg.io/matches";
	protected static final String URL_TEAMS="http://worldcup.sfg.io/teams/";
	protected static final String MATCHES="matches";
	protected static final String TEAMS="teams";
	protected static final String MATCHES_JSON_NUMBER="match_number";
	protected static final String MATCHES_JSON_LOCATION="location";
	protected static final String MATCHES_JSON_STATUS="status";
	protected static final String MATCHES_JSON_HOME_TEAM="home_team";
	protected static final String MATCHES_JSON_AWAY_TEAM="away_team";
	protected static final String MATCHES_JSON_COUNTRY="country";
	protected static final String MATCHES_JSON_COUNTRY_CODE = "code";
	protected static final String MATCHES_JSON_COUNTRY_GOALS="goals";
	protected static final String TEAMS_JSON_COUNTRY="country";
	protected static final String TEAMS_JSON_ALTERNATE_NAME="alternate_name";
	protected static final String TEAMS_JSON_FIFA_CODE="fifa_code";
	protected static final String TEAMS_JSON_GROUP_ID="group_id";



	



	


	



}
