package adidas.sdk.worldcup.utils;

/**
 * Declaration of the interface. This is responsible to advice to the app
 * when the data is loaded
 * @author jesusmiguel
 *
 */
public interface SdkResponse {
    void processFinish();
}