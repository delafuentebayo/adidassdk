package adidas.sdk.worldcup.classes;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;


/**
 * Class to manage the info of a team
 * 
 * @author jesusmiguel
 *
 */
public class Team implements Serializable{
	/**
	 * Automatically generated serialVersionUID
	 */
	private static final long serialVersionUID = 3792951590636227797L;
	//Declaration of variables
	//Declare here in case more needed.
	private String country;
	private String alternate_name;
	private String fifa_code;
	private String group_id;
	
	/**
	 * Empty Constructor
	 */
	public Team() {
		super();
	}

	/**
	 * Parametrized constructor
	 * @param country
	 * @param alternate_name
	 * @param fifa_code
	 * @param group_id
	 */
	public Team(String country, String alternate_name, String fifa_code,
			String group_id) {
		super();
		this.country = country;
		this.alternate_name = alternate_name;
		this.fifa_code = fifa_code;
		this.group_id = group_id;
	}
	
	//Getters and setters

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAlternate_name() {
		return alternate_name;
	}

	public void setAlternate_name(String alternate_name) {
		this.alternate_name = alternate_name;
	}

	public String getFifa_code() {
		return fifa_code;
	}

	public void setFifa_code(String fifa_code) {
		this.fifa_code = fifa_code;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}
	

	
	
	

}
