package adidas.sdk.worldcup.classes;

import java.io.Serializable;

/**
 * Class to manage information of a match
 *
 * @author jesusmiguel
 *
 */
public class Match implements Serializable {
	
	
	/**
	 * Automatically generated serialVersionUID
	 */
	private static final long serialVersionUID = 2134537848587421329L;
	//Declaration of needed variables.
	//Declare here in case more needed.
	private int match_number;
	private String location;
	private String status;
	private Team home_team;
	private Team away_team;
	private int home_goals;
	private int away_goals;
	
	/**
	 * Empty Constructor
	 */
	public Match() {
		super();
	}

	/**
	 * Parametrized constructor
	 * @param country
	 * @param alternate_name
	 * @param fifa_code
	 * @param group_id
	 */
	public Match(int match_number, String location, String status,
			Team home_team, Team away_team, int home_goals, int away_goals) {
		super();
		this.match_number = match_number;
		this.location = location;
		this.status = status;
		this.home_team = home_team;
		this.away_team = away_team;
		this.home_goals = home_goals;
		this.away_goals = away_goals;
	}
	
	//Getters and setters

	public int getMatch_number() {
		return match_number;
	}

	public void setMatch_number(int match_number) {
		this.match_number = match_number;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Team getHome_team() {
		return home_team;
	}

	public void setHome_team(Team home_team) {
		this.home_team = home_team;
	}

	public Team getAway_team() {
		return away_team;
	}

	public void setAway_team(Team away_team) {
		this.away_team = away_team;
	}

	public int getHome_goals() {
		return home_goals;
	}

	public void setHome_goals(int home_goals) {
		this.home_goals = home_goals;
	}

	public int getAway_goals() {
		return away_goals;
	}

	public void setAway_goals(int away_goals) {
		this.away_goals = away_goals;
	}
	
	

	
	

}
